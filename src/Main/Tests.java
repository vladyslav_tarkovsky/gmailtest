package Main;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import pages.MainPage;
import pages.NewLetterPage;




public class Tests extends WebDrInit{
	private pages.MainPage mainPage;
	private pages.LogInPage logInPage;
	private NewLetterPage newLetterPage;
		
	@Test
	public void _01_LogIn() throws InterruptedException{
		mainPage = new MainPage(driver);
		String username = mainPage.UserName.getText();
		Assert.assertEquals(username, "Vlad Tarkovsky");
		System.out.println("-------   " + username);
		String act = mainPage.Inputs.getAttribute("tabindex");
		System.out.println("-------   " + act);
		Assert.assertEquals(act, "0");
		mainPage.Exit.click();
		mainPage.Logout.click();
		
		logInPage = PageFactory.initElements(driver, pages.LogInPage.class);
		Assert.assertTrue(logInPage.NewAccBut.isEnabled());
	}
	
	@Test (groups="add")
	public void _02_NewLetter() throws InterruptedException{
		mainPage = new MainPage(driver);
		mainPage.NewLetter.click();
		Thread.sleep(1000);
		newLetterPage = new NewLetterPage(driver);
		Assert.assertTrue(newLetterPage.Send.isEnabled());
		newLetterPage.ToWhom.sendKeys("test@test.com");
		newLetterPage.Theme.sendKeys("test");
		Assert.assertTrue(newLetterPage.SaveDraft.isEnabled());
		driver.switchTo().frame(newLetterPage.EditFrame);
		for (String f : Main.Help.textFromFile("textMessLorem.txt")){
			newLetterPage.TextStr.sendKeys(f);
			newLetterPage.TextStr.sendKeys("\n");
		}
		
		driver.switchTo().defaultContent();
		newLetterPage.SaveDraft.click();
		Thread.sleep(6000);
	}
	
	@Test
	public void _03_ffghf() {
		mainPage = new MainPage(driver);
		for (WebElement s : mainPage.AddrColl()) {
			String df = s.getText();
			Boolean dfB = df.equals("�");
			if (dfB == false) {
				System.out.println(df);
				System.out.println("---------");
			}
		}
	}
}
