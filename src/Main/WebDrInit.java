package Main;
import static org.junit.Assert.fail;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;




public class WebDrInit {
	
	protected static WebDriver driver; 
	public String baseUrl;
	private StringBuffer verificationErrors = new StringBuffer();
	private pages.LogInPage logInPage;
	public WebDriverWait wait;
	protected static String Login;
	protected static String Password;
	
	@BeforeMethod 
	public void setUp() throws Exception {
		
		List<String> LogPass = Main.Help.textFromFile("login-password.txt");
		
		Login =  (String) LogPass.get(0);
		Password = (String) LogPass.get(1);

//		driver = new InternetExplorerDriver();
//		driver = new OperaDriver();
		driver = new FirefoxDriver();
//		driver = new ChromeDriver();

		baseUrl = "http://gmail.com";
		
		driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, 5);
		
		driver.get(baseUrl);
		driver.manage().window().maximize();
		logInPage = PageFactory.initElements(driver, pages.LogInPage.class);
		logInPage.Login.sendKeys(Login);
		logInPage.Password.sendKeys(Password);
		logInPage.SignInButt.click();
	}
	
	@AfterMethod
	public void tearDown() throws Exception {
//		driver.close();
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			fail(verificationErrorString);
		}

	}
}
