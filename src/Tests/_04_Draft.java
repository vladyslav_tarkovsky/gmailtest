package Tests;

import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import pages.DraftPage;
import pages.MainPage;
import Main.Help;
import Main.WebDrInit;

public class _04_Draft extends WebDrInit {
	

	@Test
	public void _04_T_Draft() throws InterruptedException {
		
		MainPage mainPage = new MainPage(driver);
		String tyu = mainPage.Draft.getText();
		mainPage.Draft.click();
		Thread.sleep(2000);
		DraftPage draft = new DraftPage(driver);
		String ghj = tyu + " - " + Login + "@gmail.com - Gmail";
		String hj = driver.getTitle();
		Assert.assertEquals(hj, ghj);
		System.out.println("Title - " + hj);
		System.out.println("ghj - " + ghj);
//		wait.until(new ExpectedCondition<Boolean>() {
//			public Boolean apply(WebDriver d) {
//				return d.getTitle().contains("hj");
//			}
//			});
		ArrayList<String> list = new ArrayList<String>();
		for (WebElement s : draft.DraftAttColl()) {
//			String[] splits = s.getText().split("[-,. ]+");
			String[] splits = Help.splitStr(s.getText(), "-,. ");
			list.add(splits[0]);
		}
		for (String f : list) {
			System.out.println(f);
		}
		System.out.println("--------------------");
				
		Boolean dfB = list.get(0).equals("test");
		if (dfB == true){
			draft.DraftCheckBoxColl().get(0).click();
			Thread.sleep(4000);
			Assert.assertTrue(draft.DelDraftButton.isDisplayed());
			Thread.sleep(2000);
			draft.DelDraftButton.click();
		}
		
		Thread.sleep(4000);
	}

}
