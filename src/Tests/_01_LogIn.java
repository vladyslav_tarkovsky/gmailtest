package Tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import Main.WebDrInit;
import pages.LogInPage;
import pages.MainPage;

public class _01_LogIn extends WebDrInit {
	

	@Test
	public void _01_T_LogIn(){
		MainPage mainPage = new MainPage(driver);
		String username = mainPage.UserName.getText();
		Assert.assertEquals(username, "Vlad Tarkovsky");
		System.out.println("-------   " + username);
		String act = mainPage.Inputs.getAttribute("tabindex");
		System.out.println("-------   " + act);
		Assert.assertEquals(act, "0");
		mainPage.Exit.click();
		mainPage.Logout.click();
		
		LogInPage logInPage = new LogInPage(driver);
		Assert.assertTrue(logInPage.NewAccBut.isEnabled());
	}
}
