package Tests;

import java.awt.AWTException;
import java.util.Calendar;
import org.testng.annotations.Test;

import Main.Help;


public class _07_Date {

	@Test
	public void _07_T_Date() throws InterruptedException, AWTException {
		
		System.out.println("date - " + Help.CurDateString());
		// Date date = new Date();
		Calendar now = Calendar.getInstance();

		System.out.println("Current date 1: " + (now.get(Calendar.MONTH) + 1)
				+ "-" + now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR) + "-" + now.get(Calendar.HOUR_OF_DAY) + ":" + now.get(Calendar.MINUTE));
		
		System.out.println("Current date 2: " + (now.get(Calendar.MONTH) + 1)
				+ "-" + now.get(Calendar.DATE) + "-" + now.get(Calendar.YEAR) + "-" + now.get(Calendar.SECOND));

		// add days to current date using Calendar.add method

		now.add(Calendar.DATE, 1);

		System.out.println("date after one day : "
				+ (now.get(Calendar.MONTH) + 1) + "-" + now.get(Calendar.DATE)
				+ "-" + now.get(Calendar.YEAR));

		// substract days from current date using Calendar.add method

		now = Calendar.getInstance();

		now.add(Calendar.DATE, -10);

		System.out.println("date before 10 days : "
				+ (now.get(Calendar.MONTH) + 1) + "-" + now.get(Calendar.DATE)
				+ "-" + now.get(Calendar.YEAR));

		// Thread.sleep(2000);
		// String name = "some_random_name";
		// driver.get("http://www.seleniumhq.org/");
		// ((JavascriptExecutor)
		// driver).executeScript("window.open(arguments[0],\"" + name + "\")",
		// "http://selenium2.ru/");
		// Thread.sleep(3000);
		System.out.println(Help.randomText());

	}
}
