package Tests;

import org.testng.Assert;
import org.testng.annotations.Test;

import Main.WebDrInit;

import pages.MainPage;
import pages.NewLetterPage;

public class _02_NewLetter extends WebDrInit {

	@Test (groups="add")
	public void _02_T_NewLetter() throws InterruptedException{
		MainPage mainPage = new MainPage(driver);
		mainPage.NewLetter.click();
		Thread.sleep(1000);
		NewLetterPage newLetterPage = new NewLetterPage(driver);
		Assert.assertTrue(newLetterPage.Send.isEnabled());
		newLetterPage.ToWhom.sendKeys("test@test.com");
		newLetterPage.Theme.sendKeys("test");
		Assert.assertTrue(newLetterPage.SaveDraft.isEnabled());
		driver.switchTo().frame(newLetterPage.EditFrame);
		for (String f : Main.Help.textFromFile("textMessLorem.txt")){
			newLetterPage.TextStr.sendKeys(f);
			newLetterPage.TextStr.sendKeys("\n");
		}
	}
}
