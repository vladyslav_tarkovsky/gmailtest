package Tests;

import java.awt.AWTException;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import Main.Help;
import Main.WebDrInit;

import pages.BinPage;
import pages.MainPage;
import pages.NewLetterPage;

public class _05_SendLetter extends WebDrInit {
	/*
	 * �������� ������� ������ "��������" ���������� ������ � ������� �� �����
	 */
	@Test
	public void _05_T_SendLetter() throws InterruptedException, AWTException {

		MainPage mainPage = new MainPage(driver);
		String subj = "test - " + Help.CurDateString();
		mainPage.sendNewMess(Login, subj);
//		mainPage.NewLetter.click();
//		Thread.sleep(1000);
//		NewLetterPage newLetterPage = new NewLetterPage(driver);
//		Assert.assertTrue(newLetterPage.Send.isEnabled());
//		String ghj = Login + "@gmail.com";
//		newLetterPage.ToWhom.sendKeys(ghj);
//		String subj = "test";
//		newLetterPage.Theme.sendKeys(subj);
//		Assert.assertTrue(newLetterPage.SaveDraft.isEnabled());
//		driver.switchTo().frame(newLetterPage.EditFrame);
//		newLetterPage.TextStr.sendKeys(Help.randomText());
////		for (String f : Main.Help.textFromFile("shortText.txt")) {
////			newLetterPage.TextStr.sendKeys(f);
////			newLetterPage.TextStr.sendKeys("\n");
////		}
//		
//		driver.switchTo().defaultContent();
//		Thread.sleep(2000);
//		newLetterPage.Send.click();
		String sendDate = Help.CurDateString();
		System.out.println("sendDate - " + sendDate);
		Thread.sleep(3000);
		int ert = 0;
		for (WebElement s : mainPage.LetterAttColl()) {
			String[] splits = Help.splitStr(s.getText(), "- ");
			if (splits[0].equals("test") == true) {
				String[] dateString = Help.splitStr(mainPage.UnreadSubjDateColl().get(ert * 2 + 1).getAttribute("title"), "., ");
				String dateFromAtt = dateString[0] + "-" + Help.MonthToInt(dateString[1]) + "-" + dateString[2] + "-" + dateString[4];
				Assert.assertEquals(dateFromAtt, sendDate);
				mainPage.InputCheckBoxColl().get(ert).click();
				Thread.sleep(1000);
				mainPage.DelLetterBut.click();
				mainPage.ElseLink.click();
				mainPage.Bin.click();
				
//				String originalWindow = driver.getWindowHandle();
//				for (String g : driver.getWindowHandles()) {
//					if (!g.equalsIgnoreCase(originalWindow)) {
//						driver.switchTo().window(g);
//						Thread.sleep(2000);
//						break;
//					}
//				}
//				System.out.println("Active Window URL is : "
//						+ driver.getTitle());
				
//				driver.close();
//				driver.switchTo().window(originalWindow);

				Thread.sleep(2000);
				BinPage bin = new BinPage(driver);
				List<WebElement> vbn = bin.UnreadBinSubjColl();
				String klkl = vbn.get(1).getText();
				System.out.println(klkl);
				Assert.assertEquals(bin.UnreadBinSubjColl().get(0).getText(), subj);
				System.out.println("title - " + bin.UnreadBinSubjDateColl().get(3).getAttribute("title"));
				String[] dateString2 = Help.splitStr(bin.UnreadBinSubjDateColl().get(3).getAttribute("title"), "., ");
				String dateFromAtt2 = dateString2[0] + "-" 	+ Help.MonthToInt(dateString2[1]) + "-" + dateString2[2] + "-" + dateString2[4];
				
				Assert.assertEquals(dateFromAtt2, sendDate);

				Thread.sleep(1000);

			}
			ert++;
		}
	}

}
