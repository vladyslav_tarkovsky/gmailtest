package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import Main.Help;


public class MainPage {
	
	private WebDriver driver;
	
	public MainPage(WebDriver _driver){
		driver = _driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "#gbi4t")
	public WebElement UserName;
	
	@FindBy(xpath = "//span/a[contains(text(), '��������')]")
	public WebElement Inputs;
	
	@FindBy(css = "#gbg4")
	public WebElement Exit;
	
	@FindBy(css = "#gb_71")
	public WebElement Logout;
	
	@FindBy(xpath = "//div[contains(text(), '��������')]")
	public WebElement NewLetter;
	
	@FindBy(css = "a[title^='���������']")
	public WebElement Draft;
	
	@FindBy(css = "div[aria-label='�������']")
	public WebElement DelLetterBut;
	
	@FindBy(xpath = "(//span[contains(text(), '���')])[2]")
	public WebElement ElseLink;
	
	@FindBy(xpath = "//a[contains(text(), '�������')]")
	public WebElement Bin;
	
	// ���
	
		
	public List<WebElement> AddrColl() { //
		return driver.findElements(By.cssSelector(".yX.xY")); //  .yP
	}
	
	public List<WebElement> InputCheckBoxColl() { // 
		return driver.findElements(By.cssSelector("div[class='BltHke nH oy8Mbf'][role='main'] div[class='T-Jo-auh']"));
	}
	
	public List<WebElement> LetterAttColl() { //
		return driver.findElements(By.cssSelector("div[class='BltHke nH oy8Mbf'][role='main'] div[class='xS']"));
	}
	
	public List<WebElement> UnreadMessColl() { // 
		return driver.findElements(By.cssSelector(".zF"));
	}
	
	public List<WebElement> UnreadSubjColl() { // 
		return driver.findElements(By.cssSelector("div[class='BltHke nH oy8Mbf'][role='main'] div[class='y6'] span b"));
	}
	
	public List<WebElement> UnreadDateColl() { // .//tr//span/b/..
		return driver.findElements(By.cssSelector("td[class^='xW xY'] b"));
	}
	
	public List<WebElement> UnreadSubjDateColl() { // 
		return driver.findElements(By.xpath("//tr//span/b/.."));
	}
	
	public void sendNewMess (String Login, String subj) throws InterruptedException{
		NewLetter.click();
		Thread.sleep(1000);
		NewLetterPage newLetterPage = new NewLetterPage(driver);
		Assert.assertTrue(newLetterPage.Send.isEnabled());
		String ghj = Login + "@gmail.com";
		newLetterPage.ToWhom.sendKeys(ghj);
//		String subj = "test - " + Help.CurDateString();
		newLetterPage.Theme.sendKeys(subj);
		Assert.assertTrue(newLetterPage.SaveDraft.isEnabled());
		driver.switchTo().frame(newLetterPage.EditFrame);
		newLetterPage.TextStr.sendKeys(Help.randomText());
		driver.switchTo().defaultContent();
		Thread.sleep(1000);
		newLetterPage.Send.click();
	}
}

