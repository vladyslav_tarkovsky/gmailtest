package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BinPage {
	
private WebDriver driver;
	
	public BinPage(WebDriver _driver){
		driver = _driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "#gbi4t")
	public WebElement UserName;
	
	public List<WebElement> UnreadBinSubjDateColl() { // 
		return driver.findElements(By.xpath("//tr//span/b/.."));
	}

	public List<WebElement> UnreadBinSubjColl() { // 
		return driver.findElements(By.cssSelector("div[class='BltHke nH oy8Mbf'][role='main'] div[class='y6'] span b"));//(By.cssSelector("div[class='y6']<span b")); (By.xpath("//div[@class='y6']/span/b"));
	}
}
