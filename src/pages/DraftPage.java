package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DraftPage {
	
private WebDriver driver;
	
	public DraftPage(WebDriver _driver){
		driver = _driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "(.//font)[1]")
	public WebElement FirstDraftTitle;
	
	@FindBy(xpath = "//div[contains(text(), '������� ���������')]")
	public WebElement DelDraftButton;
	
	public List<WebElement> DraftAttColl() { //
		return driver.findElements(By.cssSelector("div[class='BltHke nH oy8Mbf'][role='main'] div[class='xS']"));
	}
	
	public List<WebElement> DraftTitleColl() { //
		return driver.findElements(By.xpath(".//font"));
	}
	

	public List<WebElement> DraftCheckBoxColl() { //
		return driver.findElements(By.cssSelector("div[class='BltHke nH oy8Mbf'][role='main'] div[class='T-Jo-auh']"));
	}
}
