package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class LogInPage {
	
private WebDriver driver;
	
	public LogInPage(WebDriver _driver){
		driver = _driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css = "#Email")
	public WebElement Login;
	
	@FindBy(css = "#Passwd")
	public WebElement Password;
	
	@FindBy(css = "#signIn")
	public WebElement SignInButt;
	
	@FindBy(css = "#link-signup")
	public WebElement NewAccBut;

}
