package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class NewLetterPage {
	
	private WebDriver driver;
	
	public NewLetterPage(WebDriver _driver){
		driver = _driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "//div/b[contains(text(), '���������')]")  //div[role='button']:contains('���������')
	public WebElement Send;
	
	@FindBy(xpath = "//div[contains(text(), '������� ��������')]")  //div[role='button']:contains('���������')
	public WebElement DelDraft;
	
	@FindBy(name = "to")
	public WebElement ToWhom;
	
	@FindBy(name = "subject")
	public WebElement Theme;
	
	@FindBy(id = ":96")
	public WebElement LetterBody;
	
	@FindBy(xpath = "//div[contains(text(), '��������� ��������')]")
	public WebElement SaveDraft;
	
	@FindBy(css = "iframe[class = 'Am Al editable']")
	public WebElement EditFrame;
	
	@FindBy(xpath = "//body")
	public WebElement TextStr;

}
